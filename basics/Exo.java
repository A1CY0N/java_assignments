import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.List;

class MaFenetre  extends JFrame implements ActionListener{
	//Class attributes
	private JTextField text;
	private JLabel label;
	private JButton bouton;
	private String chain = "";
	private JTextField saisie;
	private JFrame pop;
	private JTextArea affichage;
	private Path fich_path;

	//Constructor
	public MaFenetre(){
		Container contenu = getContentPane();
		contenu.setLayout(new BorderLayout());
		label = new JLabel("Saisir une chaine ");
		contenu.add(label,BorderLayout.WEST);
		text = new JTextField(15);
		contenu.add(text, BorderLayout.CENTER);
		bouton = new JButton ("Copie");
		bouton.addActionListener(this);
		contenu.add(bouton,BorderLayout.SOUTH);
	}
	public void actionPerformed(ActionEvent ev){
		Object source = ev.getSource();
		if (source == bouton){
			/*String copie = text.getText() +"\n";*/
			try 
			{
				fich_path = Paths.get(text.getText());
				Charset charset = Charset.forName("UTF-8");
				/*BufferedWriter writer = Files.newBufferedWriter(fich_path, charset, StandardOpenOption.APPEND);
				writer.write(copie);
				writer.close();*/
				//BufferedReader reader = Files.newBufferedReader(fich_path, charset);
				List<String> lines = Files.readAllLines(fich_path, charset);
				for (String line : lines)
				{
					chain+=line + "\n";
					System.out.println(chain);
				}
				pop = new JFrame();
				affichage = new JTextArea (chain);
				pop.add(affichage);
				pop.setTitle("Pop");
				pop.setSize(300,80);
				pop.pack();
				pop.setVisible(true);
			}
			catch(IOException e){
				JFrame frame = new JFrame();
				System.out.println("erreur : "+ e.getMessage());
				//JOptionPane.showMessageDialog(frame,"Erreur : " + e.getMessage(),"ERREUR", ERROR_MESSAGE);
			}
		}
	}
	public void isetFile(Path fich_path)
	{
		
	}
	
}

public class Exo {
	public static void main (String[] args){
		MaFenetre fen = new MaFenetre();
		fen.setVisible(true);
		fen.pack();
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}