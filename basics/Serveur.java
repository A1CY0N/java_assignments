import java.net.*;
import java.io.*;
public class Serveur {
	final static int port = 5000;
	public static void main (String[] args)
	{
		try 
		{
			ServerSocket socketServeur = new ServerSocket(port);
			System.out.println("Launch server");
			while (true)
			{
				Socket socketClient = socketServeur.accept();
				String message = "";
				System.out.println("Connection with : "+socketClient.getInetAddress());
				BufferedReader in = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
				PrintStream out = new PrintStream(socketClient.getOutputStream());
				message = in.readLine();
				out.println(message);
				socketClient.close();
			}
		}catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

