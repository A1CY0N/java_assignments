import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.*;
import java.nio.charset.Charset;
import java.nio.file.*;

class MaFenetre  extends JFrame implements ActionListener{
	//Données Membres
	private JTextField text;
	private JLabel label;
	private JButton bouton;

	//Constructeur
	public MaFenetre(){
		Container contenu = getContentPane();
		contenu.setLayout(new BorderLayout());
		label = new JLabel("Enter a string ");
		contenu.add(label,BorderLayout.WEST);
		text = new JTextField(15);
		contenu.add(text, BorderLayout.CENTER);
		bouton = new JButton ("Write");
		bouton.addActionListener(this);
		contenu.add(bouton,BorderLayout.SOUTH);
	}
	public void actionPerformed(ActionEvent ev){
		Object source = ev.getSource();
		if (source == bouton){
			String copie = text.getText() +"\n";
			
			try {
			Path fich_path = Paths.get("/home/user/","input.txt");
			Charset charset = Charset.forName("UTF-8");
			BufferedWriter writer = Files.newBufferedWriter(fich_path, charset, StandardOpenOption.APPEND);
			writer.write(copie);
			writer.close();
			
			}
			catch(IOException e){
				JFrame frame = new JFrame();
				System.out.println("Error : "+ e.getMessage());
				//JOptionPane.showMessageDialog(frame,"Erreur : " + e.getMessage(),"ERREUR", ERROR_MESSAGE);
			}
		}
	}
	
}

public class Text {
	public static void main (String[] args){
		MaFenetre fen = new MaFenetre();
		fen.setVisible(true);
		fen.pack();
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}