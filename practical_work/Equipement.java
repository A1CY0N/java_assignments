import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.net.*;

import javax.swing.*;

public class Equipement implements ActionListener {
	private static int nb_Eqt;
	private String adrIP;
	private String adrMAC;
	private String nom_const;
	private String adrIPmin;
	private String adrIPmax;
	private String adrIPcour;
	private String type_eqt;
	private boolean mis_baie;
	private double dim_baie[];
	private JButton bouton1;
	private JButton bouton2;
	private JTextField saisie, saisieMAC, saisieIP;
	private JRadioButton routeur;
	private JRadioButton serveur;
	private JRadioButton imprimante;
	private JRadioButton NAS;
	private JRadioButton PC;
	private JRadioButton vrai;
	private JRadioButton faux;
	private JRadioButton QIP;
	private JRadioButton QMAC;
	protected JMenuItem un, deux, trois;
	private JFrame fen, fen1;
	private static JMenu m;
	public ArrayList <Equipement> al;
	
	
	public Equipement() // Constructeur
	{
		ajoutEqt();
	}
	public void ajoutEqt()
	{
		// Window element
		fen = new JFrame();
		Container contenu = fen.getContentPane();
		JPanel boiteMAC=new JPanel();
		JPanel boiteIP=new JPanel();
		saisieIP = new JTextField (20);
		saisieMAC = new JTextField (20);
		JLabel monLabelMAC = new JLabel ("Entre @ MAC", JLabel.CENTER);
		JLabel monLabelIP = new JLabel ("Entre @ IP", JLabel.CENTER);
		JLabel monLabelEQP = new JLabel ("Choisir le type d'eqt", JLabel.CENTER);
		JLabel monLabelBaie = new JLabel ("Mis en baie ?", JLabel.CENTER);
		bouton1 = new JButton("Entrer");
		
		// Adding elements
		// MAC box
		boiteMAC.add(monLabelMAC);
		boiteMAC.add(saisieMAC);
		
		// IP box
		boiteIP.add(monLabelIP);
		boiteIP.add(saisieIP);
		
		// Add MAC window box
		contenu.add(boiteMAC);
		contenu.add(boiteIP);
		
		
		// Button group 1
		ButtonGroup groupe1 = new ButtonGroup();
		routeur = new JRadioButton("routeur");
		serveur = new JRadioButton("serveur");
		imprimante = new JRadioButton("imprimante");
		NAS = new JRadioButton("NAS");
		PC = new JRadioButton("PC");
		
		groupe1.add(routeur);
		groupe1.add(serveur);
		groupe1.add(imprimante);
		groupe1.add(NAS);
		groupe1.add(PC);
		
		// Button group 2
		ButtonGroup groupe2 = new ButtonGroup();
		vrai = new JRadioButton("true");
		faux = new JRadioButton("false");
		groupe2.add(vrai);
		groupe2.add(faux);
		
		// Affichage du groupe du choix d'équipements
		contenu.add(monLabelEQP);
		contenu.add(routeur);
		contenu.add(serveur);
		contenu.add(imprimante);
		contenu.add(NAS);
		contenu.add(PC);
		
		// Affichage du choix baie
		contenu.add(monLabelBaie);
		contenu.add(vrai);
		contenu.add(faux);
		contenu.add(bouton1);
		
		// Ecoute
		saisieIP.addActionListener(this);
		saisieMAC.addActionListener(this);
		bouton1.addActionListener(this);
		
		
		// Affichage
		fen.setLayout(new GridLayout(12,1));
		fen.pack();
		fen.setTitle("EX 1");
		//fen.setVisible(true);
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
	}
	@Override
	public void actionPerformed(ActionEvent e){
		// TODO Auto-generated method stub
		Object source = e.getSource();
		if (source == bouton1)
			{
				adrMAC = saisieMAC.getText();
				adrIP = adrIPcour;
				recup_nomconst();
				afficheEqt();
				if (source == vrai)
				{
					// fenetre à définir + tard
				}
				
			}
		if (source == bouton2)
		{
			 for(int i = 0; i < al.size(); i++)
			 {
				 if (al.get(i) == saisie.getText())
				 {
					 // Affiche fenêtre
				 }
			 }
		}
		if (source == un)
		{
			ajoutEqt();
			fen.setVisible(true);
			System.out.println("toto");
		}
		if (source == deux)
		{
			afficheEqt();
			fen1.setVisible(true);
		}
		//JOptionPane.showMessageDialog(null, "I am happy !");
	}
	public void afficheEqt()
	{
		        // Element de la fenêtre
				fen1 = new JFrame();
				Container contenu = fen1.getContentPane();
				JPanel boite=new JPanel();
				saisie = new JTextField (20);
				JLabel monLabel = new JLabel ("Entre adresse IP ou MAC", JLabel.CENTER);
				bouton2 = new JButton("Entrer");
				
				// Ajout des éléments dans la boite
				boite.add(monLabel);
				boite.add(saisie);
		
				// Button group 2
				ButtonGroup groupe = new ButtonGroup();
				QIP = new JRadioButton("@ IP");
				QMAC = new JRadioButton("@ MAC");
				groupe.add(QIP);
				groupe.add(QMAC);
				
				// Add MAC or IP window box
				contenu.add(boite);			
				contenu.add(QMAC);
				contenu.add(QIP);
				contenu.add(bouton2);

				// Listening
				saisie.addActionListener(this);
					
				// Display
				fen1.setLayout(new GridLayout(12,1));
				fen1.pack();
				fen1.setTitle("Fen 1 1");
				//fen1.setVisible(true);
				fen1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
	}
	public void incr_adrIP_cour()
	{
		String[] octets = adrIPcour.split("\\.");
		for(int i = 0 ; i < octets.length ; i++)
		{
			System.out.println(octets);
		}
	}
	public void recup_nomconst()
	{
		/*String s;
	    BufferedReader r = new BufferedReader(new InputStreamReader(new URL(http://api.macvendors.com/00:11:43:00:00:01).openStream()));
	    while ((s = r.readLine()) != null) {
	    System.out.println(s);*/
	}
	
	
	
}
